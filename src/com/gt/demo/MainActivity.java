package com.gt.demo;

import com.geetest.sdk.GtDialog;
import com.geetest.sdk.GtDialog.GtListener;
import com.geetest.sdk.GtTestActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		
		findViewById(R.id.button).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				Intent intent = new Intent(getBaseContext(),GtTestActivity.class);
//				
//				startActivity(intent);
				openGtTest();

				
			}
		});
	}
	
	
	public  void openGtTest(){
		GtDialog dialog = new GtDialog(MainActivity.this);
		dialog.setGtListener(new GtListener() {
			
			@Override
			public void gtResult(boolean success, String result) {
				// TODO Auto-generated method stub
				if(success){
					//验证成功
					Toast.makeText(getBaseContext(), " 成功 result:" + result,
							Toast.LENGTH_LONG).show();
					
				}else{
					//验证失败
					Toast.makeText(getBaseContext(), " 失败 result:" + result,
							Toast.LENGTH_LONG).show();
				}
			}
			
			@Override
			public void closeGt() {
				// TODO Auto-generated method stub
				Toast.makeText(getBaseContext(), " 窗口关闭:" ,
						Toast.LENGTH_LONG).show();
			}
		});
		dialog.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
